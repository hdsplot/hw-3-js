let userNumber = +prompt("Please, type your number");

while (!Number.isInteger(userNumber)) {
  userNumber = +prompt("Please, type correct number");
}
if (userNumber >= 5) {
  for (let i = 5; i <= userNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
} else {
  console.log("Sorry, no numbers");
}
